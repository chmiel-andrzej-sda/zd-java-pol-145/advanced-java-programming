package eu.andret.sdacademy.tasks.task12;

public record Manufacturer(String name, int yearOfEstablishment, String country) {
}
