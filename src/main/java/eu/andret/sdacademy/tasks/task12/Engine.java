package eu.andret.sdacademy.tasks.task12;

public enum Engine {
	V12,
	V8,
	V6,
	I6,
	I4,
	I3
}
