package eu.andret.sdacademy.tasks.task12;

import java.util.List;

public record Car(String make, String model, double price, int yearOfProduction, List<Manufacturer> manufacturers,
				  Engine engine) {
}
