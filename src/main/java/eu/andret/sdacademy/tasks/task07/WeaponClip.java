package eu.andret.sdacademy.tasks.task07;

import java.util.ArrayList;
import java.util.List;

public class WeaponClip {
	private final List<String> bullets = new ArrayList<>();
	private final int capacity;

	public WeaponClip(final int capacity) {
		this.capacity = capacity;
	}

	public void loadBullet(final String bullet) {
		if (bullets.size() < capacity) {
			bullets.add(bullet);
		} else {
			System.out.println("Clip is full. Cannot load more bullets.");
		}
	}

	public boolean isLoaded() {
		return !bullets.isEmpty();
	}

	public void shot() {
		if (!bullets.isEmpty()) {
			final String bullet = bullets.remove(bullets.size() - 1);
			System.out.println("Shot fired: " + bullet);

		} else {
			System.out.println("Clip is empty.");
		}
	}
}
