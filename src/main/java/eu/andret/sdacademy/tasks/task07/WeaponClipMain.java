package eu.andret.sdacademy.tasks.task07;

public final class WeaponClipMain {
	public static void main(final String[] args) {
		final WeaponClip clip = new WeaponClip(5);
		clip.loadBullet("Bullet 1");
		clip.loadBullet("Bullet 2");
		clip.loadBullet("Bullet 3");


		clip.shot(); // Shot: Bullet 3, Next bullet ready: Bullet 2
		clip.shot(); // Shot: Bullet 2, Next bullet ready: Bullet 1
		clip.shot(); // Shot: Bullet 1, Clip is empty.
		clip.shot(); // Clip is empty.
	}
}
