package eu.andret.sdacademy.tasks.task21;

import java.util.Objects;

public class Cube extends Shape3D {
	private final double a;

	public Cube(final double a) {
		this.a = a;
	}

	@Override
	public double calculatePerimeter() {
		return a * 12;
	}

	@Override
	public double calculateArea() {
		return a * a * 6;
	}

	@Override
	public double calculateVolume() {
		return a * a * a;
	}

	@Override
	public boolean equals(final Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		final Cube cube = (Cube) o;
		return Double.compare(cube.a, a) == 0;
	}

	@Override
	public int hashCode() {
		return Objects.hash(a);
	}

	@Override
	public String toString() {
		return "Cube{" +
				"a=" + a +
				"} " + super.toString();
	}
}
