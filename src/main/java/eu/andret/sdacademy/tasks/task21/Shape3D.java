package eu.andret.sdacademy.tasks.task21;

import eu.andret.sdacademy.tasks.task20.Shape;
import eu.andret.sdacademy.tasks.task22.FillStatus;
import eu.andret.sdacademy.tasks.task22.Fillable;

public abstract class Shape3D extends Shape implements Fillable {
	public abstract double calculateVolume();

	@Override
	public FillStatus fill(final double fluid) {
		if (fluid < calculateVolume()) {
			return FillStatus.NOT_ENOUGH;
		}
		if (fluid > calculateVolume()) {
			return FillStatus.TOO_MUCH;
		}
		return FillStatus.ENOUGH;
	}
}
