package eu.andret.sdacademy.tasks.task21;

import java.util.Objects;

public class Cone extends Shape3D {
	private final double r;
	private final double l;
	private final double h;

	public Cone(final double r, final double l, final double h) {
		this.r = r;
		this.l = l;
		this.h = h;
	}

	@Override
	public double calculateVolume() {
		return Math.PI * r * r * h / 3;
	}

	@Override
	public double calculatePerimeter() {
		return Math.PI * r * 2;
	}

	@Override
	public double calculateArea() {
		return Math.PI * r * l;
	}

	@Override
	public boolean equals(final Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		final Cone cone = (Cone) o;
		return Double.compare(cone.r, r) == 0 && Double.compare(cone.l, l) == 0 && Double.compare(cone.h, h) == 0;
	}

	@Override
	public int hashCode() {
		return Objects.hash(r, l, h);
	}

	@Override
	public String toString() {
		return "Cone{" +
				"r=" + r +
				", l=" + l +
				", h=" + h +
				"} " + super.toString();
	}
}
