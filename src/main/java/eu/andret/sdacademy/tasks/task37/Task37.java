package eu.andret.sdacademy.tasks.task37;

import eu.andret.sdacademy.tasks.task36.ThreadPlaygroundRunnable;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * IGNORED
 */
public final class Task37 {
	public static void main(final String[] args) {
		final ExecutorService executorService = Executors.newFixedThreadPool(2);
		for (int i = 0; i < 10; i++) {
			executorService.submit(new ThreadPlaygroundRunnable("name-" + i));
		}
	}
}
