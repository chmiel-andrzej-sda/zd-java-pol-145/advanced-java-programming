package eu.andret.sdacademy.tasks.task05;

import java.util.HashSet;
import java.util.Set;

/**
 * IGNORED
 */
public class SDAHashSet<E> {
	private final Set<E> set = new HashSet<>();

	public int size() {
		return set.size();
	}

	public boolean contains(final E o) {
		return set.contains(o);
	}

	public boolean add(final E e) {
		return set.add(e);
	}

	public boolean remove(final E o) {
		return set.remove(o);
	}

	public void clear() {
		set.clear();
	}
}
