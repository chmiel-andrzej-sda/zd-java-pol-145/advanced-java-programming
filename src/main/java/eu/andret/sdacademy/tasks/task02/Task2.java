package eu.andret.sdacademy.tasks.task02;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public final class Task2 {
	public static void main(final String[] args) {
		final List<String> list = new ArrayList<>();
		list.add("AAA");
		list.add("Baaa");
		list.add("bbb");
		list.add("Zzz");
		list.add("ffff");

		reverseSortStream(list).forEach(System.out::println);
		reverseSortModifying(list);
		System.out.println(list);
	}

	public static List<String> reverseSortStream(final List<String> list) {
		return list.stream()
				.sorted(Comparator.comparing(String::toLowerCase, Comparator.reverseOrder()))
				.toList();
	}

	public static void reverseSortModifying(final List<String> list) {
		list.sort(Comparator.comparing(String::toLowerCase, Comparator.reverseOrder()));
	}
}
