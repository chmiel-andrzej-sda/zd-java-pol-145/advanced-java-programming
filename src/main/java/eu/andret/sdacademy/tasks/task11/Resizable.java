package eu.andret.sdacademy.tasks.task11;

public interface Resizable {
	Resizable resize(double resizeFactor);
}
