package eu.andret.sdacademy.tasks.task29;

import java.util.Arrays;
import java.util.function.Predicate;

public final class PartOfMain {
	public static void main(final String[] args) {
		final Integer[] array = {1, 2, 3, 4, 5};
		System.out.println(partOf(array, x -> x % 2 == 0));
	}

	public static <T> double partOf(final T[] array, final Predicate<T> predicate) {
		final long passedTest = Arrays.stream(array)
				.filter(predicate)
				.count();

		return 100.0 * passedTest / array.length;
	}
}
