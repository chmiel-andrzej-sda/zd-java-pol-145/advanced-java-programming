package eu.andret.sdacademy.tasks.task09;

import eu.andret.sdacademy.tasks.task10.Movable;
import eu.andret.sdacademy.tasks.task10.MoveDirection;
import eu.andret.sdacademy.tasks.task11.Resizable;

public record Circle(Point2D center, Point2D point) implements Movable, Resizable {
	public double getRadius() {
		return Math.sqrt(Math.pow(point.x() - center.x(), 2) + Math.pow(point.y() - center.y(), 2));
	}

	public double getPerimeter() {
		return getRadius() * Math.PI * 2;
	}

	public double getArea() {
		return getRadius() * Math.pow(Math.PI, 2);
	}

	@Override
	public Circle move(final MoveDirection moveDirection) {
		return new Circle(center.move(moveDirection), point.move(moveDirection));
	}

	@Override
	public Circle resize(final double resizeFactor) {
		return new Circle(
				new Point2D(center.x() * resizeFactor, center.y() * resizeFactor),
				new Point2D(point.x() * resizeFactor, point.y() * resizeFactor));
	}
}
