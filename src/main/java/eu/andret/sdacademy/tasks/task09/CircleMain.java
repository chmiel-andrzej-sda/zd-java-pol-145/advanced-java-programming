package eu.andret.sdacademy.tasks.task09;

public final class CircleMain {
	public static void main(final String[] args) {
		final Point2D center = new Point2D(0.0, 0.0);
		final Point2D point = new Point2D(3.0, 4.0);
		final Circle circle = new Circle(center, point);
		System.out.println(circle.getRadius());
		System.out.println(circle.getPerimeter());
		System.out.println(circle.getArea());
	}
}
