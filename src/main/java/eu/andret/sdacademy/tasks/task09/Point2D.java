package eu.andret.sdacademy.tasks.task09;

import eu.andret.sdacademy.tasks.task10.Movable;
import eu.andret.sdacademy.tasks.task10.MoveDirection;

public record Point2D(double x, double y) implements Movable {
	@Override
	public Point2D move(final MoveDirection moveDirection) {
		return new Point2D(x + moveDirection.x(), y + moveDirection.y());
	}
}
