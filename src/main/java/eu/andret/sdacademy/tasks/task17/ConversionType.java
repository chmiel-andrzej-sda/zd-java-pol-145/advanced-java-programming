package eu.andret.sdacademy.tasks.task17;

public enum ConversionType {
	METERS_TO_YARDS(value -> 1.094f * value),
	YARDS_TO_METERS(value -> 0.91f * value),
	CENTIMETERS_TO_INCHES(value -> 0.39f * value),
	INCHES_TO_CENTIMETERS(value -> 2.5f * value),
	KILOMETERS_TO_MILES(value -> 0.62f * value),
	MILES_TO_KILOMETERS(value -> 1.61f * value);

	private final Converter converter;

	ConversionType(final Converter value) {
		converter = value;
	}

	public Converter getConverter() {
		return converter;
	}
}
