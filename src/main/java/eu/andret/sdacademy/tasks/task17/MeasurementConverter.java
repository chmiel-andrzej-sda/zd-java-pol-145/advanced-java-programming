package eu.andret.sdacademy.tasks.task17;

public class MeasurementConverter {
	public float convert(final float value, final ConversionType conversionType) {
		return conversionType.getConverter().convert(value);
	}
}
