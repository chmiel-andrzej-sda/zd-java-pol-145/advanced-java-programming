package eu.andret.sdacademy.tasks.task17;

@FunctionalInterface
public interface Converter {
	float convert(float value);
}
