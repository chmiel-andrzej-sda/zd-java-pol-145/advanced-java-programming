package eu.andret.sdacademy.tasks.task25;

public class BasketEmptyException extends Exception {
	public BasketEmptyException(final String s) {
		super(s);
	}
}
