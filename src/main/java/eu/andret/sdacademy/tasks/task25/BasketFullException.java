package eu.andret.sdacademy.tasks.task25;

public class BasketFullException extends Exception {
	public BasketFullException(final String s) {
		super(s);
	}
}
