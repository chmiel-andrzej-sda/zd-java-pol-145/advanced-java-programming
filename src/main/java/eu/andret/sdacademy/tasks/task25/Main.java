package eu.andret.sdacademy.tasks.task25;

public class Main {
	public static void main(final String[] args) throws BasketFullException, BasketEmptyException {
		final Basket basket1 = new Basket();
		basket1.addToBasket();
		basket1.removeFromBasket();
	}
}
