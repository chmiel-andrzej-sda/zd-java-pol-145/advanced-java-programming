package eu.andret.sdacademy.tasks.task25;

public class Basket {
	private int elements;

	public void addToBasket() throws BasketFullException {
		if (elements >= 10) {
			throw new BasketFullException("Nie ma miejsca!");
		}
		elements++;
	}

	public void removeFromBasket() throws BasketEmptyException {
		if (elements == 0) {
			throw new BasketEmptyException("Nie ma żadnych elementów!");
		}
		elements--;
	}
}
