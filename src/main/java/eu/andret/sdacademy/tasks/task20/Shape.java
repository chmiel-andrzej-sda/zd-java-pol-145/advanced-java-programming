package eu.andret.sdacademy.tasks.task20;

public abstract class Shape {
	public abstract double calculatePerimeter();

	public abstract double calculateArea();
}
