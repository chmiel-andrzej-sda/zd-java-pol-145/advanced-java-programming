package eu.andret.sdacademy.tasks.task20;

import java.util.Objects;

public class Rectangle extends Shape {
	private final double width;
	private final double height;

	public Rectangle(final double width, final double height) {
		this.width = width;
		this.height = height;
	}

	@Override
	public double calculatePerimeter() {
		return 2 * (width + height);
	}

	@Override
	public double calculateArea() {
		return width * height;
	}

	@Override
	public boolean equals(final Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		final Rectangle rectangle = (Rectangle) o;
		return Double.compare(rectangle.width, width) == 0 && Double.compare(rectangle.height, height) == 0;
	}

	@Override
	public int hashCode() {
		return Objects.hash(width, height);
	}

	@Override
	public String toString() {
		return "Rectangle{" +
				"width=" + width +
				", height=" + height +
				"} " + super.toString();
	}
}
