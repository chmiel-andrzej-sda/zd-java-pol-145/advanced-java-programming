package eu.andret.sdacademy.tasks.task20;

import java.util.Objects;

public class Hexagon extends Shape {
	private final double side;

	public Hexagon(final double side) {
		this.side = side;
	}

	@Override
	public double calculatePerimeter() {
		return 6 * side;
	}

	@Override
	public double calculateArea() {
		return 3 * Math.sqrt(3) * side * side / 2;
	}

	@Override
	public boolean equals(final Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		final Hexagon hexagon = (Hexagon) o;
		return Double.compare(hexagon.side, side) == 0;
	}

	@Override
	public int hashCode() {
		return Objects.hash(side);
	}

	@Override
	public String toString() {
		return "Hexagon{" +
				"side=" + side +
				"} " + super.toString();
	}
}
