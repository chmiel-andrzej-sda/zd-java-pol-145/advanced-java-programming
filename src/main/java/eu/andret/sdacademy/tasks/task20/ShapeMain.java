package eu.andret.sdacademy.tasks.task20;

public final class ShapeMain {
	public static void main(final String[] args) {
		final Rectangle rectangle = new Rectangle(4, 5);
		System.out.println("Rectangle Perimeter: " + rectangle.calculatePerimeter());
		System.out.println("Rectangle Area: " + rectangle.calculateArea());


		final Triangle triangle = new Triangle(3, 4, 5);
		System.out.println("Triangle Perimeter: " + triangle.calculatePerimeter());
		System.out.println("Triangle Area: " + triangle.calculateArea());

		final Hexagon hexagon = new Hexagon(2);
		System.out.println("Hexagon Perimeter: " + hexagon.calculatePerimeter());
		System.out.println("Hexagon Area: " + hexagon.calculateArea());
	}
}
