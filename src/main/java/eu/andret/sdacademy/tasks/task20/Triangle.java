package eu.andret.sdacademy.tasks.task20;

import java.util.Objects;

public class Triangle extends Shape {
	private final double side1;

	private final double side2;

	private final double side3;

	public Triangle(final double side1, final double side2, final double side3) {
		this.side1 = side1;
		this.side2 = side2;
		this.side3 = side3;
	}

	@Override
	public double calculatePerimeter() {
		return side1 + side2 + side3;
	}

	@Override
	public double calculateArea() {
		final double s = (side1 + side2 + side3) / 2;
		return Math.sqrt(s * (s - side1) * (s - side2) * (s - side3));
	}

	@Override
	public boolean equals(final Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		final Triangle triangle = (Triangle) o;
		return Double.compare(triangle.side1, side1) == 0 && Double.compare(triangle.side2, side2) == 0 && Double.compare(triangle.side3, side3) == 0;
	}

	@Override
	public int hashCode() {
		return Objects.hash(side1, side2, side3);
	}

	@Override
	public String toString() {
		return "Triangle{" +
				"side1=" + side1 +
				", side2=" + side2 +
				", side3=" + side3 +
				"} " + super.toString();
	}
}
