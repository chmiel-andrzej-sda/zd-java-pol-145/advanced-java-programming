package eu.andret.sdacademy.tasks.task08;

public record Parcel(int lengthX, int lengthY, int lengthZ, float weight, boolean isExpress) {
}
