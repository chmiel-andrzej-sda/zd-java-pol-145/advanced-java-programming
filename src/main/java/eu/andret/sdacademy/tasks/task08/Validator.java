package eu.andret.sdacademy.tasks.task08;

public interface Validator {
	boolean validate(Parcel input);
}
