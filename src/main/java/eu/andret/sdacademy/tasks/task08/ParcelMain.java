package eu.andret.sdacademy.tasks.task08;

public final class ParcelMain {
	public static void main(final String[] args) {
		final Parcel parcel1 = new Parcel(12, 12, 12, 22.3f, true);
		final Parcel parcel2 = new Parcel(44, 12, 12, 22.3f, true);
		final Validator validator = input -> {
			if (input.lengthX() + input.lengthY() + input.lengthZ() >= 300) {
				return false;
			}
			if (input.lengthX() < 30 && input.lengthY() < 30 && input.lengthZ() < 30) {
				return false;
			}
			if (input.isExpress() && input.weight() > 15) {
				return false;
			}
			return input.isExpress() || input.weight() <= 30;
		};
		System.out.println(validator.validate(parcel1));
		System.out.println(validator.validate(parcel2));
	}
}
