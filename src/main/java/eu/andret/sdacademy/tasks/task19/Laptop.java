package eu.andret.sdacademy.tasks.task19;

import eu.andret.sdacademy.tasks.task18.Computer;

import java.util.Objects;

public class Laptop extends Computer {
	private String battery;

	public Laptop(final String processor, final String ram, final String displayCard, final String manufacturer, final String brand, final String battery) {
		super(processor, ram, displayCard, manufacturer, brand);
		this.battery = battery;
	}

	public String getBattery() {
		return battery;
	}

	public void setBattery(final String battery) {
		this.battery = battery;
	}

	@Override
	public String toString() {
		return "Laptop { " +
				"battery = '" + battery + '\'' +
				"} " + super.toString();
	}

	@Override
	public boolean equals(final Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		if (!super.equals(o)) {
			return false;
		}
		final Laptop laptop = (Laptop) o;
		return Objects.equals(battery, laptop.battery);
	}

	@Override
	public int hashCode() {
		return Objects.hash(super.hashCode(), battery);
	}
}
