package eu.andret.sdacademy.tasks.task13;

import eu.andret.sdacademy.tasks.task12.Car;
import eu.andret.sdacademy.tasks.task12.Engine;
import eu.andret.sdacademy.tasks.task12.Manufacturer;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class CarService {
	private final List<Car> cars = new ArrayList<>();

	public void addCar(final Car car) {
		cars.add(car);
	}

	public void removeCar(final Car car) {
		cars.remove(car);
	}

	public List<Car> showList() {
		return cars;
	}

	public List<Car> findV12() {
		return cars.stream()
				.filter(car -> car.engine().equals(Engine.V12))
				.toList();
	}

	public List<Car> findUnderYear(final int year) {
		return cars.stream()
				.filter(car -> car.yearOfProduction() < year)
				.toList();
	}

	public Car mostExpensive() {
		return cars.stream()
				.max(Comparator.comparingDouble(Car::price))
				.orElse(null);
	}

	public Car leastExpensive() {
		return cars.stream()
				.min(Comparator.comparingDouble(Car::price))
				.orElse(null);
	}

	public List<Car> findByManufacturers(final int numberOfManufacturers) {
		return cars.stream()
				.filter(car -> car.manufacturers().size() > numberOfManufacturers)
				.toList();
	}

	public List<Car> sortAscDesc(final boolean ascending) {
		final Comparator<Car> comparator;
		if (ascending) {
			comparator = Comparator.comparingDouble(Car::price);
		} else {
			comparator = Comparator.comparingDouble(Car::price).reversed();

		}
		return cars.stream()
				.sorted(comparator).toList();
	}

	public boolean isPresent(final Car car) {
		return cars.contains(car);
	}

	public List<Car> byManufacturer(final Manufacturer manufacturer) {
		return cars.stream()
				.filter(car -> car.manufacturers().contains(manufacturer))
				.toList();
	}

	public List<Car> byManufacturerAndYear(final int year) {
		return cars.stream()
				.filter(car -> car.manufacturers()
						.stream()
						.anyMatch(manufacturer -> manufacturer.yearOfEstablishment() == year))
				.toList();
	}
}
