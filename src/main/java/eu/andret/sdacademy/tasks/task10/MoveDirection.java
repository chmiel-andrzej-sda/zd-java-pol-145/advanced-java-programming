package eu.andret.sdacademy.tasks.task10;

public record MoveDirection(double x, double y) {
}
