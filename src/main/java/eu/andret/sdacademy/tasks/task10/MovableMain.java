package eu.andret.sdacademy.tasks.task10;

import eu.andret.sdacademy.tasks.task09.Circle;
import eu.andret.sdacademy.tasks.task09.Point2D;

public final class MovableMain {
	public static void main(final String[] args) {
		final Point2D center = new Point2D(0.0, 0.0);
		final Point2D point = new Point2D(3.0, 4.0);
		final Circle circle = new Circle(center, point);
		System.out.println(circle.getRadius());
		System.out.println(circle.getPerimeter());
		System.out.println(circle.getArea());

		final MoveDirection moveDirection = new MoveDirection(1.0, 5.0);
		System.out.println(center.move(moveDirection));
		System.out.println(circle.move(moveDirection));

		final Circle move = circle.move(moveDirection);
		System.out.println(move.getRadius());
		System.out.println(move.getPerimeter());
		System.out.println(move.getArea());

	}
}
