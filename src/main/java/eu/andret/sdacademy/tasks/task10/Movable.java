package eu.andret.sdacademy.tasks.task10;

public interface Movable {
	Movable move(MoveDirection moveDirection);
}
