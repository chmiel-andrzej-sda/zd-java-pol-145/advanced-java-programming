package eu.andret.sdacademy.tasks.task15;

public enum Car {
	FERRARI(true, 1, 1),
	PORSCHE(true, 2, 3),
	MERCEDES(true, 5, 2),
	BMW(true, 6, 3),
	OPEL(false, 5, 6),
	FIAT(false, 8, 1),
	TOYOTA(false, 6, 4);

	private final boolean isPremium;
	private final double price;
	private final int power;

	Car(final boolean isPremium, final double price, final int power) {
		this.isPremium = isPremium;
		this.price = price;
		this.power = power;
	}

	public boolean isFasterThan(final Car otherCar) {
		return compareTo(otherCar) > 0;
	}

	public boolean isPremium() {
		return isPremium;
	}

	public double getPrice() {
		return price;
	}

	public int getPower() {
		return power;
	}
}
