package eu.andret.sdacademy.tasks.task30;

import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.stream.Collectors;

public final class ReverseIO {
	public static void main(final String[] args) throws IOException {
		final String fileName = "fileName";
		final String fileExtension = ".txt";
		final String collect = Files.readAllLines(Paths.get(fileName + fileExtension))
				.stream()
				.map(StringBuilder::new)
				.map(StringBuilder::reverse)
				.map(StringBuilder::toString)
				.collect(Collectors.joining("\n"));
		try (final PrintWriter printWriter = new PrintWriter(new StringBuilder(fileName).reverse() + fileExtension)) {
			printWriter.write(collect);
		}
	}
}
