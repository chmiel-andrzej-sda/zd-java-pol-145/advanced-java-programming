package eu.andret.sdacademy.tasks.task33;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.stream.Stream;

/**
 * IGNORED
 */
public final class Task33 {
	public static void main(final String[] args) {
		try (final Stream<Path> walkStream = Files.walk(Paths.get("C:\\Users\\Andret\\OneDrive\\Images"))) {
			walkStream
					.filter(p -> p.toFile().isFile())
					.forEach(f -> {
						if (f.toString().endsWith(".jpg") || f.toString().endsWith(".png")) {
							System.out.println(f);
						}
					});
		} catch (final IOException e) {
			throw new RuntimeException(e);
		}
	}
}
