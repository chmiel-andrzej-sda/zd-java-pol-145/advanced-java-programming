package eu.andret.sdacademy.tasks.task31;

import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * IGNORED
 */
public final class Task31 {
	public static void main(final String[] args) throws IOException {
		final Path path = Paths.get("ex31.txt");
		final List<String> strings = Files.readAllLines(path);
		try (final PrintWriter printWriter = new PrintWriter("out.txt")) {
			strings.stream()
					.map(string -> string.split("\\W+"))
					.flatMap(Arrays::stream)
					.collect(Collectors.groupingBy(Function.identity(), Collectors.counting()))
					.entrySet()
					.stream()
					.map(entry -> String.format("%d\t%s", entry.getValue(), entry.getKey()))
					.forEach(printWriter::println);
		}
	}
}
