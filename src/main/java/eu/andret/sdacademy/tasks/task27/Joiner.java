package eu.andret.sdacademy.tasks.task27;

import java.util.Arrays;
import java.util.stream.Collectors;

public class Joiner<T> {
	private final String separator;

	public Joiner(final String separator) {
		this.separator = separator;
	}

	public String joinWithStream(final T... variables) {
		return Arrays.stream(variables).
				map(Object::toString)
				.collect(Collectors.joining(separator));

	}
}
