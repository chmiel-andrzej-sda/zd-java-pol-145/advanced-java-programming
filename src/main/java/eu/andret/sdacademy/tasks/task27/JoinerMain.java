package eu.andret.sdacademy.tasks.task27;

public final class JoinerMain {
	public static void main(final String[] args) {
		final Joiner<Integer> joiner = new Joiner<>("-");
		System.out.println(joiner.joinWithStream(1, 2, 3, 4, 5));
	}
}
