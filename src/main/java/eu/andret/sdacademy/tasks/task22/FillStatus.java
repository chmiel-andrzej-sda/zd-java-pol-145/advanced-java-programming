package eu.andret.sdacademy.tasks.task22;

public enum FillStatus {
	NOT_ENOUGH, ENOUGH, TOO_MUCH
}
