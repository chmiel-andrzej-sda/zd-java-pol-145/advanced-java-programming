package eu.andret.sdacademy.tasks.task22;

@FunctionalInterface
public interface Fillable {
	FillStatus fill(double filling);
}
