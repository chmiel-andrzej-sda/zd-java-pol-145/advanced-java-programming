package eu.andret.sdacademy.tasks.task38;

/**
 * IGNORED
 */
public final class CoffeeMachineMain {
	public static void main(final String[] args) throws InterruptedException {
		final CoffeeMachine coffeeMachine = new CoffeeMachine(0.0f);

		final Thread threadMakeCoffee = new Thread(() -> {
			try {
				for (int i = 0; i < 10; i++) {
					coffeeMachine.makeCoffee();
					System.out.println("Enjoy your coffee!");
				}
			} catch (final InterruptedException e) {
				Thread.currentThread().interrupt();
			}
		});

		final Thread threadRefillWater = new Thread(() -> {
			for (int i = 0; i < 10; i++) {
				coffeeMachine.fillWaterTank(0.2f);
				System.out.println("Water tank was refilled");
			}
		});

		threadMakeCoffee.start();
		threadRefillWater.start();
	}

}
