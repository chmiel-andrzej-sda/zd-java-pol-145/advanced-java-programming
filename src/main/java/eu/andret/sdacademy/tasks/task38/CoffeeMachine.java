package eu.andret.sdacademy.tasks.task38;

public class CoffeeMachine {
	private static final float TANK_VOLUME = 3.0f;
	private float volumeFilledWithWater;

	public CoffeeMachine(final float volumeFilledWithWater) {
		if (volumeFilledWithWater <= TANK_VOLUME) {
			this.volumeFilledWithWater = volumeFilledWithWater;
		} else {
			this.volumeFilledWithWater = 0.0f;
		}
	}

	public synchronized void makeCoffee() throws InterruptedException {
		Thread.sleep(200);
		if (volumeFilledWithWater >= 0.2f) {
			volumeFilledWithWater -= 0.2f;
		}
	}

	public synchronized void fillWaterTank(final float volume) {
		if (volumeFilledWithWater == TANK_VOLUME) {
			System.out.println("Water tank is full!");
		} else if (volume > TANK_VOLUME - volumeFilledWithWater) {
			volumeFilledWithWater = TANK_VOLUME;
		} else {
			volumeFilledWithWater += volume;
		}
	}
}
