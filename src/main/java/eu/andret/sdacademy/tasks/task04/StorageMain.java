package eu.andret.sdacademy.tasks.task04;

public final class StorageMain {
	public static void main(final String[] args) {
		final Storage storage = new Storage();
		storage.addToStorage("key", "value");
		storage.addToStorage("key", "value4");
		storage.addToStorage("key", "value3");
		storage.addToStorage("key", "value1");

		storage.printValues("key");
		storage.findValues("value4");
	}
}
