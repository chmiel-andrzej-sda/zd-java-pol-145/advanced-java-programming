package eu.andret.sdacademy.tasks.task04;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Storage {
	private final Map<String, List<String>> map = new HashMap<>();

	public void addToStorage(final String key, final String value) {
		if (!map.containsKey(key)) {
			map.put(key, new ArrayList<>());
		}
		map.get(key).add(value);
	}

	public void printValues(final String key) {
		System.out.println(map.get(key));
	}

	public void findValues(final String value) {
		for (final Map.Entry<String, List<String>> entry : map.entrySet()) {
			if (entry.getValue().contains(value)) {
				System.out.println(entry.getKey());
			}
		}
	}
}
