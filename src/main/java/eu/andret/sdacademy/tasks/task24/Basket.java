package eu.andret.sdacademy.tasks.task24;

public class Basket {
	private int elements;

	public void addToBasket() {
		if (elements >= 10) {
			throw new BasketFullException("Nie ma miejsca!");
		}
		elements++;
	}

	public void removeFromBasket() {
		if (elements == 0) {
			throw new BasketEmptyException("Nie ma żadnych elementów!");
		}
		elements--;
	}
}
