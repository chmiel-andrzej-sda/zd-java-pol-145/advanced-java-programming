package eu.andret.sdacademy.tasks.task24;

public final class BasketMain {
	public static void main(final String[] args) {
		final Basket basket = new Basket();
		try {
			basket.removeFromBasket();
			basket.addToBasket();
		} catch (final BasketEmptyException | BasketFullException exception) {
			System.out.println(exception.getMessage());
		}
	}
}
