package eu.andret.sdacademy.tasks.task24;

public class BasketFullException extends RuntimeException {
	public BasketFullException(final String s) {
		super(s);
	}
}
