package eu.andret.sdacademy.tasks.task24;

public class BasketEmptyException extends RuntimeException {
	public BasketEmptyException(final String s) {
		super(s);
	}
}
