package eu.andret.sdacademy.tasks.task14;

import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public final class Task14 {
	public static void main(final String[] args) {
		final Random random = new Random();
		final List<Integer> integers = Stream.generate(() -> random.nextInt(100000))
				.limit(100000)
				.toList();

		final List<Integer> distinct = integers.stream()
				.distinct()
				.toList();
		System.out.println(distinct);

		integers.stream()
				.collect(Collectors.groupingBy(integer -> integer, Collectors.counting()))
				.entrySet()
				.stream()
				.sorted((o1, o2) -> Long.compare(o2.getValue(), o1.getValue()))
				.limit(25)
				.forEach(System.out::println);
	}
}
