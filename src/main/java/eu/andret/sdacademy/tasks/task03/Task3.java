package eu.andret.sdacademy.tasks.task03;

import java.util.Map;
import java.util.stream.Collectors;

public final class Task3 {
	public static void main(final String[] args) {
		final Map<String, Integer> elements = Map.ofEntries(
				Map.entry("Java", 18),
				Map.entry("Python", 1),
				Map.entry("PHP", 0));
		mapReader(elements);
		printMap(elements);
	}

	public static void mapReader(final Map<String, Integer> map) {
		int counter = 1;
		for (final Map.Entry<String, Integer> entry : map.entrySet()) {
			if (counter == map.size()) {
				System.out.printf("Klucz: %s Wartość: %d.%n", entry.getKey(), entry.getValue());
			} else {
				System.out.printf("Klucz: %s Wartość: %d,%n", entry.getKey(), entry.getValue());
			}
			counter++;
		}
	}

	public static void printMap(final Map<String, Integer> map) {
		final String collect = map.entrySet()
				.stream()
				.map(entry -> String.format("Klucz: %s, Wartość: %d", entry.getKey(), entry.getValue()))
				.collect(Collectors.joining(",\n")) + ".";
		System.out.println(collect);
	}
}
