package eu.andret.sdacademy.tasks.task06;

import java.util.TreeMap;

public final class Task6 {
	public static void main(final String[] args) {
		final TreeMap<Character, Integer> treeMap = new TreeMap<>();
		treeMap.put('t', 21);
		treeMap.put('f', 65);
		treeMap.put('z', 58);

		System.out.println(treeMap);
		showFirstAndLastEntrySet(treeMap);
	}

	public static void showFirstAndLastEntrySet(final TreeMap<?, ?> treeMap) {
		System.out.println(treeMap.firstEntry());
		System.out.println(treeMap.lastEntry());
	}
}
