package eu.andret.sdacademy.tasks.task01;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public final class Task1 {
	public static void main(final String[] args) {
		final List<String> list = new ArrayList<>();
		list.add("AAA");
		list.add("aaa");
		list.add("bbb");
		list.add("zzz");
		list.add("ffff");

		reverseSortStream(list).forEach(System.out::println);
		reverseSortModifying(list);
		System.out.println(list);
	}

	public static List<String> reverseSortStream(final List<String> list) {
		return list.stream()
				.sorted(Comparator.reverseOrder())
				.toList();
	}

	public static void reverseSortModifying(final List<String> list) {
		list.sort(String.CASE_INSENSITIVE_ORDER.reversed());
	}
}
