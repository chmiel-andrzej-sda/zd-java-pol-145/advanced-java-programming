package eu.andret.sdacademy.tasks.task32;

import eu.andret.sdacademy.tasks.task26.Car;
import eu.andret.sdacademy.tasks.task26.CarType;
import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

/**
 * IGNORED
 */
public final class Task32 {
	public static void main(final String[] args) throws FileNotFoundException {
		final Car car1 = new Car("BMW", "description1", CarType.SEDAN);
		final Car car2 = new Car("Mercedes", "description2", CarType.CABRIO);
		final Car car3 = new Car("Audi", "description3", CarType.COUPE);
		final Car car4 = new Car("Toyota", "description4", CarType.HATCHBACK);

		final JSONObject jsonObject1 = new JSONObject();
		jsonObject1.put("name", car1.name());
		jsonObject1.put("description", car1.description());
		jsonObject1.put("type", car1.carType());

		final JSONObject jsonObject2 = new JSONObject();
		jsonObject2.put("name", car2.name());
		jsonObject2.put("description", car2.description());
		jsonObject2.put("type", car2.carType());

		final JSONObject jsonObject3 = new JSONObject();
		jsonObject3.put("name", car3.name());
		jsonObject3.put("description", car3.description());
		jsonObject3.put("type", car3.carType());

		final JSONObject jsonObject4 = new JSONObject();
		jsonObject4.put("name", car4.name());
		jsonObject4.put("description", car4.description());
		jsonObject4.put("type", car4.carType());
		final List<JSONObject> jsonObjectList = new ArrayList<>(List.of(jsonObject1,
				jsonObject2,
				jsonObject3,
				jsonObject4));

		final JSONArray jsonArray = new JSONArray(jsonObjectList);
		System.out.println(jsonArray);

		final Path path = Paths.get("files", "zadanie32.txt");
		try (final PrintWriter printWriter = new PrintWriter(path.toFile())) {
			printWriter.print(jsonArray);
		}

		final List<Car> newCarList = new ArrayList<>();

		final JSONArray jsonArrayRead = new JSONArray(new JSONTokener(new FileInputStream(path.toFile())));

		for (int i = 0; i < jsonArrayRead.length(); i++) {
			final String name = jsonArrayRead.getJSONObject(i).getString("name");
			final String description = jsonArrayRead.getJSONObject(i).getString("description");
			final CarType type = CarType.valueOf(jsonArrayRead.getJSONObject(i).getString("type"));

			newCarList.add(new Car(name, description, type));
		}

		newCarList.forEach(System.out::println);
	}
}
