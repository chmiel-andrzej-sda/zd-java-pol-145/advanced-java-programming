package eu.andret.sdacademy.tasks.task18;

import java.util.Objects;

public class Computer {
	private final String processor;
	private final String ram;
	private final String displayCard;
	private final String manufacturer;
	private final String brand;

	public Computer(final String processor, final String ram, final String displayCard, final String manufacturer, final String brand) {
		this.processor = processor;
		this.ram = ram;
		this.displayCard = displayCard;
		this.manufacturer = manufacturer;
		this.brand = brand;
	}

	public String getProcessor() {
		return processor;
	}

	public String getRam() {
		return ram;
	}

	public String getDisplayCard() {
		return displayCard;
	}

	public String getManufacturer() {
		return manufacturer;
	}

	public String getBrand() {
		return brand;
	}

	@Override
	public boolean equals(final Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		final Computer computer = (Computer) o;
		return Objects.equals(getProcessor(), computer.getProcessor()) && Objects.equals(getRam(), computer.getRam()) && Objects.equals(getDisplayCard(), computer.getDisplayCard()) && Objects.equals(getManufacturer(), computer.getManufacturer()) && Objects.equals(getBrand(), computer.getBrand());
	}

	@Override
	public int hashCode() {
		return Objects.hash(getProcessor(), getRam(), getDisplayCard(), getManufacturer(), getBrand());
	}
}
