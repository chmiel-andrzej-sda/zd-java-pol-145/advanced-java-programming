package eu.andret.sdacademy.tasks.task16;

import java.util.Arrays;

/**
 * IGNORED
 */
public enum Runner {
	BEGINNER(512, Integer.MAX_VALUE),
	INTERMEDIATE(256, 511),
	ADVANCED(Integer.MIN_VALUE, 255);

	private final int minTime;
	private final int maxTime;

	Runner(final int minTime, final int maxTime) {
		this.minTime = minTime;
		this.maxTime = maxTime;
	}

	public static Runner getFitnessLevel(final int runningTime) {
		return Arrays.stream(values())
				.filter(runner -> runner.minTime <= runningTime && runningTime <= runner.maxTime)
				.findAny()
				.orElse(null);
	}
}
