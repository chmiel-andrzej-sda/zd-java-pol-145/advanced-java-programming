package eu.andret.sdacademy.tasks.task28;

import java.util.ArrayList;
import java.util.List;

/**
 * IGNORED
 */
public class SelectiveArrayList<E> extends ArrayList<E> {
	public List<E> getEveryNthElement(final int startIndex, final int skip) {
		final List<E> result = new ArrayList<>();
		for (int i = startIndex; i < size(); i += skip + 1) {
			result.add(get(i));
		}
		return result;
	}
}
