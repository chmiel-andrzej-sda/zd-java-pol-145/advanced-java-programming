package eu.andret.sdacademy.tasks.task28;

public final class NthElementMain {
	public static void main(final String[] args) {
		final SelectiveArrayList<String> strings = new SelectiveArrayList<>();
		strings.add("a");
		strings.add("b");
		strings.add("c");
		strings.add("d");
		strings.add("e");
		strings.add("f");
		strings.add("g");
		System.out.println(strings);
		System.out.println(strings.getEveryNthElement(2, 3));
	}
}
