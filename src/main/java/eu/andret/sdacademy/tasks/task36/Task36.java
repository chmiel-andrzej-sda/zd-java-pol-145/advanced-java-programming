package eu.andret.sdacademy.tasks.task36;

/**
 * IGNORED
 */
public class Task36 {
	private static Thread THREAD1;
	private static Thread THREAD2;

	public static void main(final String[] args) {
		THREAD1 = new Thread(new ThreadPlaygroundRunnable("test"));
		THREAD2 = new Thread(new ThreadPlaygroundRunnable("example"));

		THREAD1.start();
		THREAD2.start();
	}
}
