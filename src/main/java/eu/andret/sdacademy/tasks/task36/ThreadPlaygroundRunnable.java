package eu.andret.sdacademy.tasks.task36;

public class ThreadPlaygroundRunnable implements Runnable {
	private final String name;

	public ThreadPlaygroundRunnable(final String name) {
		this.name = name;
	}

	@Override
	public void run() {
		for (int i = 0; i < 10; i++) {
			System.out.printf("%s - %d - %s%n", Thread.currentThread().getName(), i, name);
		}
	}
}
