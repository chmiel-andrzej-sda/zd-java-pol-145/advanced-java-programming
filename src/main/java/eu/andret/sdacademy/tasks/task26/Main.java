package eu.andret.sdacademy.tasks.task26;

import java.util.Collection;
import java.util.List;

public final class Main {
	public static void main(final String[] args) {
		final List<Manufacturer> manufacturers = List.of(
				new Manufacturer("Manufacturer1", 1950, List.of(
						new Model("Dodge", 1914, List.of(
								new Car("Charger", "My dream car.", CarType.SEDAN),
								new Car("Challenger", "Not bad.", CarType.CABRIO)
						)),
						new Model("Chevrolet", 2020, List.of(
								new Car("Camaro", "My second dream car.", CarType.CABRIO),
								new Car("Corvette", "Well maybe.", CarType.COUPE)
						))
				)),
				new Manufacturer("Manufacturer2", 1988, List.of(
						new Model("BMW", 2023, List.of(
								new Car("X3", "Ahh BMW", CarType.SEDAN),
								new Car("X7", "Yup.", CarType.COUPE)
						)),
						new Model("Ford", 2008, List.of(
								new Car("Focus", "I have this one.", CarType.HATCHBACK),
								new Car("Fiesta", "YAAAS", CarType.CABRIO)
						))
				)));

		System.out.println("1. Lista wszystkich modeli.");
		manufacturers.stream()
				.map(Manufacturer::models)
				.flatMap(Collection::stream)
				.forEach(System.out::println);

		System.out.println("2. Lista wszystkich aut.");
		manufacturers.stream()
				.map(Manufacturer::models)
				.flatMap(Collection::stream)
				.map(Model::cars)
				.flatMap(Collection::stream)
				.forEach(System.out::println);

		System.out.println("3. Lista wszystkich nazw producentów.");
		manufacturers.stream()
				.map(Manufacturer::name)
				.forEach(System.out::println);

		System.out.println("4. Lista wszystkich lat założenia producentów.");
		manufacturers.stream()
				.map(Manufacturer::yearOfCreation)
				.forEach(System.out::println);

		System.out.println("5. Lista wszystkich nazw modeli.");
		manufacturers.stream()
				.map(Manufacturer::models)
				.flatMap(Collection::stream)
				.map(Model::name)
				.forEach(System.out::println);

		System.out.println("6. Lista wszystkich lat startu produkcji.");
		manufacturers.stream()
				.map(Manufacturer::models)
				.flatMap(Collection::stream)
				.map(Model::productionStartYear)
				.forEach(System.out::println);

		System.out.println("7. Lista wszystkich nazw aut.");
		manufacturers.stream()
				.map(Manufacturer::models)
				.flatMap(Collection::stream)
				.map(Model::cars)
				.flatMap(Collection::stream)
				.map(Car::name)
				.forEach(System.out::println);

		System.out.println("8. Lista wszystkich opisów aut.");
		manufacturers.stream()
				.map(Manufacturer::models)
				.flatMap(Collection::stream)
				.map(Model::cars)
				.flatMap(Collection::stream)
				.map(Car::description)
				.forEach(System.out::println);

		System.out.println("9. Modele z parzystym rokiem produkcji.");
		manufacturers.stream()
				.map(Manufacturer::models)
				.flatMap(Collection::stream)
				.filter(model -> model.productionStartYear() % 2 == 0)
				.map(Model::name)
				.forEach(System.out::println);

		System.out.println("10. Modele z parzystym rokiem założenia.");
		manufacturers.stream()
				.filter(manufacturer -> manufacturer.yearOfCreation() % 2 == 0)
				.map(Manufacturer::models)
				.flatMap(Collection::stream)
				.map(Model::cars)
				.flatMap(Collection::stream)
				.map(Car::name)
				.forEach(System.out::println);

		System.out.println("11. Modele z parzystym rokiem produkcji oraz nieparzystym rokiem założenia.");
		manufacturers.stream()
				.filter(manufacturer -> manufacturer.yearOfCreation() % 2 == 1)
				.map(Manufacturer::models)
				.flatMap(Collection::stream)
				.filter(model -> model.productionStartYear() % 2 == 0)
				.map(Model::cars)
				.flatMap(Collection::stream)
				.map(Car::name)
				.forEach(System.out::println);

		System.out.println("12. Tylko auta typu CABRIO z nieparzystym rokiem startu produkcji modelu i parzystym rokiem założenia producenta.");
		manufacturers.stream()
				.filter(manufacturer -> manufacturer.yearOfCreation() % 2 == 0)
				.map(Manufacturer::models)
				.flatMap(Collection::stream)
				.filter(model -> model.productionStartYear() % 2 != 0)
				.map(Model::cars)
				.flatMap(Collection::stream)
				.filter(car -> car.carType().equals(CarType.CABRIO))
				.map(Car::name)
				.forEach(System.out::println);

		System.out.println("13. Tylko auta typu SEDAN z modelu nowszego niż 2019 oraz rokiem założenia producenta mniejszym niż 1919.");
		manufacturers.stream()
				.filter(manufacturer -> manufacturer.yearOfCreation() < 1919)
				.map(Manufacturer::models)
				.flatMap(Collection::stream)
				.filter(model -> model.productionStartYear() > 2019)
				.map(Model::cars)
				.flatMap(Collection::stream)
				.filter(car -> car.carType().equals(CarType.SEDAN))
				.map(Car::name)
				.forEach(System.out::println);
	}
}
