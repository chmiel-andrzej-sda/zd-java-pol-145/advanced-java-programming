package eu.andret.sdacademy.tasks.task26;

import java.util.List;

public record Manufacturer(String name, int yearOfCreation, List<Model> models) {
}
