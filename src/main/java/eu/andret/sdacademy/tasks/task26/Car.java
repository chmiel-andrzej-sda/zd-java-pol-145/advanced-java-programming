package eu.andret.sdacademy.tasks.task26;

public record Car(String name, String description, CarType carType) {
}
