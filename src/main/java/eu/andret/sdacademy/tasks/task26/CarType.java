package eu.andret.sdacademy.tasks.task26;

public enum CarType {
	COUPE,
	CABRIO,
	SEDAN,
	HATCHBACK
}
